defmodule LogLevel do
  def to_label(level, legacy?) do
    case {level, legacy?} do
      {0, false} -> :trace
      {1, _} -> :debug
      {2, _} -> :info
      {3, _} -> :warning
      {4, _} -> :error
      {5, false} -> :fatal
      _ -> :unknown
    end
  end

  def alert_recipient(level, legacy?) do
    # targetvalue = to_label(level, legacy?)
    log = to_label(level, legacy?)

    cond do
      log == :error or log == :fatal -> :ops
      log == :unknown and legacy? -> :dev1
      log == :unknown -> :dev2
      true -> false
    end

    # case {level, legacy?} do
    #   {4, _}-> :ops
    #   {5, _}-> :ops

    #   {_, true}-> :dev1
    #   {_, false}-> :dev2
    # "error" -> return :ops
    # "fatal" -> return :ops
    # "unknown" -> return :dev1
    # "unknown" -> return :dev2
    # _->return "nothing"
    # end
  end
end
