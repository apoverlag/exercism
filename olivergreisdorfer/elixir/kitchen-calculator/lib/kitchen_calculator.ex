defmodule KitchenCalculator do
  def get_volume(volume_pair) do
    elem(volume_pair, 1)
  end

  def to_milliliter({unit, n}) do
    case unit do
      :milliliter -> {:milliliter, n}
      :cup -> {:milliliter, n * 240}
      :teaspoon -> {:milliliter, n * 5}
      :tablespoon -> {:milliliter, n * 15}
      :fluid_ounce -> {:milliliter, n * 30}
    end
  end

  def from_milliliter({unit, n}, new_unit) do
    case {unit, new_unit} do
      {:milliliter, :milliliter} -> {:milliliter, n}
      {:milliliter, :cup} -> {:cup, n / 240}
      {:milliliter, :teaspoon} -> {:teaspoon, n / 5}
      {:milliliter, :tablespoon} -> {:tablespoon, n / 15}
      {:milliliter, :fluid_ounce} -> {:fluid_ounce, n / 30}
    end
  end

  def convert(volume_pair, unit) do
    volume_pair
    |> to_milliliter()
    |> from_milliliter(unit)
  end
end
