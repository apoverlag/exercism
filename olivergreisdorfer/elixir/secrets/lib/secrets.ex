defmodule Secrets do
  def secret_add(secret) do
    fn a -> a + secret end
  end

  def secret_subtract(secret) do
    fn a -> a - secret end
  end

  def secret_multiply(secret) do
    fn a -> a * secret end
  end

  def secret_divide(secret) do
    fn a -> div(a, secret) end
  end

  def secret_and(secret) do
    use Bitwise, only_operators: true
    fn a -> a &&& secret end
  end

  def secret_xor(secret) do
    use Bitwise
    fn a -> bxor(a, secret) end
  end

  def secret_combine(fn1, fn2) do
    fn x -> x |> fn1.() |> fn2.() end
  end
end
