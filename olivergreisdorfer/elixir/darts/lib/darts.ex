defmodule Darts do
  @type position :: {number, number}

  @doc """
  Calculate the score of a single dart hitting a target
  """
  @spec score(position :: position) :: integer
  def score({x, y}) do
    distance_from_center = :math.sqrt(x * x + y * y)
    # inner radius: 1
    cond do
      distance_from_center <= 1 -> 10
      # middle radius: 5
      distance_from_center <= 5 -> 5
      # outer radius: 10
      distance_from_center <= 10 -> 1
      true -> 0
    end
  end
end
