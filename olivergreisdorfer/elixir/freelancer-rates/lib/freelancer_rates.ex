defmodule FreelancerRates do
  def daily_rate(hourly_rate) do
    hourly_rate * 8.0
  end

  def apply_discount(before_discount, discount) do
    before_discount * (1 - discount / 100.0)
  end

  def monthly_rate(hourly_rate, discount) do
    monthlyRate = daily_rate(hourly_rate) * 22.0
    monthlyRateAfterDiscount = apply_discount(monthlyRate, discount)
    # transform into int, round to the next >= number
    trunc(Float.ceil(monthlyRateAfterDiscount))
  end

  def days_in_budget(budget, hourly_rate, discount) do
    dailyRate = daily_rate(hourly_rate)
    dailyRateAfterDiscount = apply_discount(dailyRate, discount)
    affordableDays = budget / dailyRateAfterDiscount
    # round to the next <= number
    Float.floor(affordableDays, 1)
  end
end
