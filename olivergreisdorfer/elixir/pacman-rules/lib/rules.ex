defmodule Rules do
  # older version kept for completion's sake
  def eat_ghost?(power_pellet_active, touching_ghost) do
    power_pellet_active and touching_ghost
    # case {power_pellet_active, touching_ghost} do
    #   {true, true} -> true
    #   _ -> false
    # end
  end

  def score?(touching_power_pellet, touching_dot) do
    touching_power_pellet or touching_dot
    # case {touching_power_pellet, touching_dot} do
    #   {false, false} -> false
    #   _ -> true
    # end
  end

  def lose?(power_pellet_active, touching_ghost) do
    !power_pellet_active and touching_ghost
    # case {power_pellet_active, touching_ghost} do
    #   {false, true} -> true
    #   _ -> false
    # end
  end

  def win?(has_eaten_all_dots, power_pellet_active, touching_ghost) do
    has_eaten_all_dots and !lose?(power_pellet_active, touching_ghost)
    # case {has_eaten_all_dots, power_pellet_active, touching_ghost} do
    #   # when alive
    #   {true, true, true} -> true
    #   {true, true, false} -> true
    #   {true, false, false} -> true

    #   # when dead
    #   {true, false, true} -> false
    #   {false, false, true} -> false

    #   # else
    #   _ -> false
    # end
  end
end
