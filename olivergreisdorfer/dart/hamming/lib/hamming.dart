class Hamming {
  int distance(String stringOne, String stringTwo) {
    List<String> listOne = stringOne.split('');
    List<String> listTwo = stringTwo.split('');

    if (listOne.length != listTwo.length) {
      if (listOne.length != listTwo.length) {
        if (listOne.isEmpty || listTwo.isEmpty)
          throw ArgumentError("no strand must be empty");
        else
          throw ArgumentError("left and right strands must be of equal length");
      }
    }

    int counter = 0;
    for (var i = 0; i < listOne.length; i++) {
      if ((listOne[i] != listTwo[i])) {
        counter++;
      }
    }
    return counter;
  }
}
