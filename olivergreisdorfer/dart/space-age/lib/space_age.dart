class SpaceAge {
  double convertToPlanetYear(String planet, double seconds) {
    double earthYearEquivalent = seconds / 31557600;
    double yearResult = earthYearEquivalent;

    switch (planet) {
      case "Mercury":
        yearResult = earthYearEquivalent / 0.2408467;
        break;
      case "Venus":
        yearResult = earthYearEquivalent / 0.61519726;
        break;
      case "Mars":
        yearResult = earthYearEquivalent / 1.8808158;
        break;
      case "Jupiter":
        yearResult = earthYearEquivalent / 11.862615;
        break;
      case "Saturn":
        yearResult = earthYearEquivalent / 29.447498;
        break;
      case "Uranus":
        yearResult = earthYearEquivalent / 84.016846;
        break;
      case "Neptune":
        yearResult = earthYearEquivalent / 164.79132;
        break;

      default:
    }

    return yearResult;
  }

  double age({required String planet, required double seconds}) {
    double result = convertToPlanetYear(planet, seconds);
    double parsedResult = double.parse((result).toStringAsFixed(2));
    return parsedResult;
  }
}
