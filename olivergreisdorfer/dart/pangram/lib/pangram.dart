class Pangram {
  bool isPangram(String sentence) {
    List<String> alphabet = [
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "i",
      "j",
      "k",
      "l",
      "m",
      "n",
      "o",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z"
    ];

    for (String letter in alphabet) {
      if (!sentence.toLowerCase().contains(letter)) return false;
    }

    return true;
  }
}