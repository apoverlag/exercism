class BeerSong {
  List<String> recite(int numberOfBottles, int numberOfIterations) {

    String firstLine;
    String secondLine;
    String emptyString = '';
    List<String> resultArray = [];


    for ( int i = 0; i < numberOfIterations; i++) {

        // pls codereview this

        // if (numberOfBottles == 2) {
        //   firstLine = "this is the first line if bootles = 2";
        //   // firstLine = "$numberOfBottles bottles of beer on the wall, $numberOfBottles bottles of beer.";
        //   secondLine = 'Take one down and pass it around, one bottle of beer on the wall.';
        // }
        // if (numberOfBottles == 1) {
        //   firstLine = '1 bottle of beer on the wall, 1 bottle of beer.';
        //   secondLine = 'Take it down and pass it around, no more bottles of beer on the wall.';
        // }
        // if (numberOfBottles == 0) {
        //   firstLine = "No more bottles of beer on the wall, no more bottles of beer.";
        //   secondLine = 'Go to the store and buy some more, 99 bottles of beer on the wall.';
        // }
        // else {
        //   firstLine = "$numberOfBottles bottles of beer on the wall, $numberOfBottles bottles of beer.";
        //   secondLine = 'Take one down and pass it around, ${numberOfBottles - 1} bottles of beer on the wall.';
        // }

        switch (numberOfBottles) {
          case 0:
            firstLine = "No more bottles of beer on the wall, no more bottles of beer.";
            secondLine = 'Go to the store and buy some more, 99 bottles of beer on the wall.';
          break;
          case 1:
            firstLine = '1 bottle of beer on the wall, 1 bottle of beer.';
            secondLine = 'Take it down and pass it around, no more bottles of beer on the wall.';
          break;
          case 2:
            firstLine = "$numberOfBottles bottles of beer on the wall, $numberOfBottles bottles of beer.";
            secondLine = 'Take one down and pass it around, 1 bottle of beer on the wall.';
          break;
          default:
            firstLine = "$numberOfBottles bottles of beer on the wall, $numberOfBottles bottles of beer.";
            secondLine = 'Take one down and pass it around, ${numberOfBottles - 1} bottles of beer on the wall.';
          break;
        }
        numberOfBottles -= 1;
        resultArray.add(firstLine);
        resultArray.add(secondLine);
        if (numberOfIterations > 1 && numberOfIterations - 1 > i){
          resultArray.add(emptyString);
        }
      }

    return resultArray;
    }
  }
