Sebastian Göttschkes 🏳️‍🌈
======================

# Manifesto for Agile Software Development

Individuals and interactions over processes and tools
Working software over comprehensive documentation
Customer collaboration over contract negotiation
Responding to change over following a plan

If you think SCRUM is agile, read the manifesto again
