defmodule FreelancerRates do
  def daily_rate(hourly_rate), do: hourly_rate * 8.0

  def apply_discount(before_discount, discount),
    do: before_discount - before_discount * discount / 100

  def monthly_rate(hourly_rate, discount) do
    total_rate = daily_rate(hourly_rate) * 22.0
    ceil(apply_discount(total_rate, discount))
  end

  def days_in_budget(budget, hourly_rate, discount),
    do: (budget / apply_discount(daily_rate(hourly_rate), discount)) |> Float.floor(1)
end
