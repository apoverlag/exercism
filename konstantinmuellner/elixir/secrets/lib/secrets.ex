defmodule Secrets do
  def secret_add(secret) do
    fn addSecret ->
      addSecret + secret
    end
  end

  def secret_subtract(secret) do
    fn subSecret ->
      subSecret - secret
    end
  end

  def secret_multiply(secret) do
    fn multiSecret ->
      multiSecret * secret
    end
  end

  def secret_divide(secret) do
    fn divideSecret ->
      div(divideSecret, secret)
    end
  end

  def secret_and(secret) do
    fn andSecret ->
      Bitwise.&&&(andSecret, secret)
    end
  end

  def secret_xor(secret) do
    fn xorSecret ->
      Bitwise.^^^(xorSecret, secret)
    end
  end

  def secret_combine(secret_function1, secret_function2) do
    fn combobreaker ->
      secret_function2.(secret_function1.(combobreaker))
    end
  end
end
