apoverlag/exercism
==================

# A few words

This repository and our use of exercism is aimed at improving our coding abilities. Make sure to not only solve the exercises, but improve your code, seek mentors within the company and from exercism. Don't look at other answers in the beginning and rather ask for help to enable you to solve the exercises on your own. This isn't a work task you need to complete, this is an invitiation to grow your abilities.

## Rules

A few (soft) rules:

1. Work within your exercism folder (see below) and refrain from directly changing other peoples code without their permission
2. No commits to main. Create a branch, push and do a MR
3. Submit either work in progress or working exercism code. Please refrain from using this repo for anything else
4. If asked for feedback, be kind
5. Continuous improvement for this repo is welcome. If you want to improve the overall structure, tools or processes, talk to the team
6. Only basic MRs (setup, hello world) should be merged without comments and iterations. All others should be discussed by multiple people
7. Ask forgiveness, not permission

# Getting started

Let's get you started with exercism @ ApoVerlag 😊

## Setup

In order to work with exercism, you'll need an exercism account and the exercism cli:

1. Create an exercism account at https://exercism.org/users/sign_up either using your GitHub account or by entering your information
2. Follow the guide to install the exercism CLI: https://exercism.org/cli-walkthrough
3. Configure the exercism CLI with your token: https://exercism.org/docs/using/solving-exercises/working-locally

Next, we need to create our exercism folder within this repository and configure exercism to download exercises there:

1. Create a new branch called something like "setup/YOURNAME", with YOURNAME being replaced by your full name (see point 2 for a naming scheme)
2. Create a new folder in the root directory of this repository, calling it with your full name, all lowercase, no spaces. For example, "Sebastian Göttschkes" would become "sebastiangoettschkes". We'll call this directory "your exercism directory" from now on
3. Create a README.md file in your exercism directory, containing a headline with your name and, if you like, other content. Please remember that your are commiting into a public repository used for work, so no secret sharing 😉
4. Commit the README.md and push your branch. Create a MR agains main.
5. Configure the exercism workspace: `exercism configure --workspace PATHTOYOURFOLDER`. For example, if your repository is checked out at "~/workspace/exercism" and your name is "Jane Doe", you would run "exercism configure --workspace ~/workspace/exercism/janedoe"

For an example of a PR created within these steps, see https://gitlab.com/apoverlag/exercism/-/merge_requests/2

## Start with your first track

Exercism is organized in tracks, one for each language. Within each track, multiple exercises are available for you to work through. As an example, we'll complete the "Hello World" exercise in the Javascript track:

1. Make sure your PR from the setup was merged
2. Change to the main branch, pull from origin and create a new branch, naming it something like "exercise/YOURNAME-js-hello-word"
3. Join the Javascript track: https://exercism.org/tracks/javascript
4. Navigate to the first exercise, called "Hello world": https://exercism.org/tracks/javascript/exercises/hello-world
5. On the right side, you see the cli command to download the exercise. Execute it
6. Navigate into your exercism folder. You should see a new folder "javascript/hello-word". Change into this directory
7. Follow the instructions on how to solve this exercise. You should find a README, a HELP file and multiple other material to tell you how to setup the exercise, how to run the tests and what to do to solve it
8. After the tests pass, submit your exercise `exercism submit hello-world.js`
9. Ater your solution was accepted, commit content from the hello-world folder. Make sure to check the file list for any files which should not be commited (modules, ...) and exclude them
10. Create a MR against main

For an example of a PR created within these steps, see https://gitlab.com/apoverlag/exercism/-/merge_requests/3
