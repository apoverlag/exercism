FROM node:17.4-alpine

ENV HOME=/opt/app/ TERM=xterm

RUN \
    apk update && \
    apk add bash && \
    rm -rf /var/cache/apk/*

WORKDIR /opt/app
COPY . .
