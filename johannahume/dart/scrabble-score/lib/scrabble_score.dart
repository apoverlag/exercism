
int score(String wordCount) {
  const letterScores = {
    1: ['a', 'e', 'i', 'o', 'u', 'l', 'n', 'r', 's', 't'],
    2: ['d', 'g'],
    3: ['b', 'c', 'm', 'p'],
    4: ['f', 'h', 'v', 'w', 'y'],
    5: ['k'],
    8: ['j', 'x'],
    10: ['q', 'z']
  };
  if (wordCount == '') return 0;
  var score = 0;
  wordCount.split('').forEach((letter){
  letterScores.forEach((x, y){
      if (y.contains(letter.toLowerCase())){
        score += x;
      };
  });
});
  return score;
}