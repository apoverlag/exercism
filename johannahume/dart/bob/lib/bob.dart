class Bob {
  String response(String input) {
    input = input.trim();
    bool hasLetters = input.contains(RegExp(r"[a-zA-z]"));
    bool uppercaseInput = identical(input, input.toUpperCase()) && hasLetters;
    if (input == "") return "Fine. Be that way!";
    if (input.substring(input.length - 1) == "?" && uppercaseInput)
      return "Calm down, I know what I'm doing!";
    if (input.substring(input.length - 1) == "?") return "Sure.";
    if (uppercaseInput) return "Whoa, chill out!";
    return "Whatever.";
  }
}
