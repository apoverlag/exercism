class Leap {
  bool leapYear(int date) {
    return (date % 400 == 0) || (date % 4 == 0 && date % 100 != 0);
  }
}

// other solution: 
// class Leap {
//  bool leapYear(int year){
//     if (year % 400 == 0){
//       return true;
//     }
//     if (year % 100 == 0){
//       return false;
//     }
//     if (year % 4 == 0){
//       return true;
//     }
//     else {
//       return false;
//     }
//   }
// }

