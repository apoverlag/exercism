String twoFer([String? name]) {
  if (name == null) {
    return "One for you, one for me.";
  } else {
    return "One for $name, one for me.";
  }
}

// Alternatice Solution:
// String twoFer([String? name]) => "One for ${name ?? "you"}, one for me.";