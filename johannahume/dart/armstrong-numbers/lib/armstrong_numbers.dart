import 'dart:math';

class ArmstrongNumbers {
  bool isArmstrongNumber(int number) {
    List<String> digits = number.toString().split('');
    num sum = 0;
    for (int i = 0; i < digits.length; i++) {
      num currentDigit = num.parse(digits[i]);
      sum += pow(currentDigit, digits.length);
    }
    if (sum == number) {
      return true;
    }
    return false;
  }
}
