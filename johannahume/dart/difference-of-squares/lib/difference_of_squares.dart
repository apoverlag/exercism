class DifferenceOfSquares {
  int sumOfSquares(int number) {
    int sum1 = 0;
    for (int i = 0; i <= number; i++) {
      sum1 = sum1 + i * i;
    }
    return sum1;
  }

  int squareOfSum(int number) {
    int sum2 = 0;
    for (int i = 0; i <= number; i++) {
      sum2 = sum2 + i;
    }
    return sum2 * sum2;
  }

  int differenceOfSquares(int number) {
    int sum1 = sumOfSquares(number);
    int sum2 = squareOfSum(number);

    return (sum1 - sum2).abs();
  }
}
