defmodule LogLevel do
  def to_label(level, legacy?) do
    label_codes = %{0 => :trace, 1 => :debug, 2 => :info, 3 => :warning, 4 => :error, 5 => :fatal}

    if not Map.has_key?(label_codes, level) or (legacy? and level in [0, 5]) do
      :unknown
    else
      label_codes[level]
    end

    ## other solution:
    # if Map.has_key?(label_codes, level) do
    #   if legacy? and (level == 0 or level == 5) do
    #     :unknown
    #   else
    #     label_codes[level]
    #   end
    # else
    #   :unknown
    # end
  end

  def alert_recipient(level, legacy?) do
    label = to_label(level, legacy?)

    cond do
      label in [:fatal, :error] -> :ops
      label == :unknown -> if legacy?, do: :dev1, else: :dev2
      true -> nil
    end

    ## other solution:
    #  if label in [:fatal, :error] do
    #   :ops
    #  else
    #   if label == :unknown do
    #     if legacy? do
    #       :dev1
    #     else
    #      :dev2
    #     end
    #    else
    #      nil
    #    end
    #  end
  end
end
